package com.mm.poc.kafka.consumer.beans;

public class PersonBean 
{
	private String firstName;
	private String lastName;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return String.format("FirstName: '%s' LastName: '%s'", firstName, lastName);
	}	
}

package com.mm.poc.kafka.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

import com.mm.poc.kafka.consumer.beans.PersonBean;

@SpringBootApplication
public class KafkaConsumerApplication 
{
	private static final Logger logger = LoggerFactory.getLogger(KafkaConsumerApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(KafkaConsumerApplication.class, args);
	}
	
	@KafkaListener(topics = "${message.topic.name}", groupId = "test1", containerFactory = "messageKafkaListenerContainerFactory")
	public void listenForMessage(String message) 
	{
		if(logger.isInfoEnabled()) {
			logger.info(String.format("Received Messasge in group test1: %s", message));
		}
	}	

	@KafkaListener(topics = "${person.topic.name}", groupId = "test1", containerFactory = "personKafkaListenerContainerFactory")
	public void listenForPerson(PersonBean person) 
	{
		if(logger.isInfoEnabled()) {
			logger.info(String.format("Received Person in group test1: %s", person));
		}
	}
}
